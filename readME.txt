#######################################################################################
################################ Presentation du projet ###############################
#######################################################################################

Ce fichier présente le projet tensions réalisé dans le cadre de mon premier poste de 
chargé d'études. 
Nous nous sommes inspirés de l'étude de la DARES des métiers en tension.
Ici se trouve une partie des codes que j'ai réalisé. 

Le dossier ne contient aucune donnée mais est censé contenir les tables brutes.
Le dossier scripts contient les scripts.
Le dossier intermediate contient les résultats intermédiaires pouvant être supprimés. 
Le dossier output contient les résultats finaux.

