
#########################################################################################################
#########################################################################################################
#########################################################################################################
############################################ SCRIPT TEST ################################################
#########################################################################################################
#########################################################################################################
#########################################################################################################

# Auteur : Romain Deng--Guéret
# Objectif : Mener des tests pour contrôler le bon fonctionnement du code

rm(list=ls()) # vider environnemment R pour éviter conflit
gc() # mémoire

#########################################################################################################
#################################### LISTE DES PACKAGES  ################################################
#########################################################################################################

library(testthat) # tests unitaires
library(dplyr) # manipulation de données
library(arrow) # format parquet
library(here) # gestion des chemins

#########################################################################################################
############################################ LES CHEMINS ################################################
#########################################################################################################

data_path <- here("data")
scripts_path <- here("scripts")
intermediate_path <- here("intermediate")
output_path <- here("output")

#########################################################################################################
############################################ LES DONNEES ################################################
#########################################################################################################

titanic_survival <- read_csv(file.path(data_path, "titanic.csv"))

#########################################################################################################
############################################## LES TESTS ################################################
#########################################################################################################

# Chargement de la fonction
source(file.path(scripts_path, "fonction.R"))

# Tests unitaires
test_that("get_stat_desc works", {
  
  # get_stat_desc est bien une fonction
  expect_true(inherits(get_stat_desc, "function"))
  
  # quiet ressort une liste avec result, warnings, output, messages
  quiet_get_stat_desc <- purrr::quietly(get_stat_desc)
  
  # l'objet en sortie est bien un data.frame
  expect_true(inherits(quiet_get_stat_desc(df = titanic_survival)$result, "data.frame"))
  
  # les warnings affichent bien que les NA sont retirees
  expect_warning(get_stat_desc(df = titanic_survival),
                 regexp = "argument is not numeric or logical: returning NA"
  )
  
  # le tableau ressorti a bien trois colonnes
  expect_equal(nrow(quiet_get_stat_desc(df = titanic_survival)$result), 3)
})







