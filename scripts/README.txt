
######################################################################
############################# DOCUMENTATION ##########################
######################################################################

Auteur : Romain Deng--Guéret
Etude réalisée : Métiers en tension au sein du SSM Défense  

Le script init installe les packages nécessaires pour le projet et crée les dossiers data, intermediate, output.
Le script filtrage_selection.R réalise un prétraitement des bases stockées en .parquet.
Le script analyses.R réalise des visualisations des données et tables et les exporte. 
Le script romain_repro_fiches.R calcule des statistiques descriptives.
Le script main appelle les quatre précédents scripts pour les faire exécuter dans leur intégralité.
Le script chartegraph.R créée un graphique et le met sous charte graphique.
Le script pyrage.R créé une pyramide des âges (et parallélise les calculs avec future et furrr). 
Le fichier fonction.R créée une fonction sur laquelle je veux faire des tests unitaires.
Le script generation_rapports.R génère des rapports Rmd de manière automatique à l'aide d'un fichier .yaml. 
Le script test met en place des tests pour assurer la pérennité du projet et contrôler les diverses erreurs. 
