#######################################################################################
################################ Presentation du dossier ##############################
#######################################################################################

Ici sont exportés les résultats principaux de l'étude. 
Je n'ai conservé que les codes que je considère comme intéressants. 


####################### template_rapport.Rmd ############################

Le code de ce fichier permet de générer automatiquement des rapports par code fap. 
Ce fichier est un template servant à générer des rapports en bouclant sur le code fap entré comme paramètre du fichier yaml associé.


